'use strict';

const generateFreeLog = require('./base');

module.exports = (o) => generateFreeLog(window.WebSocket, window.Promise, rpc);
