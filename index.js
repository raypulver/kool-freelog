'use strict';

const { w3cwebsocket: WebSocket } = require('websocket');
const generateGetLogs = require('./base');

module.exports = (rpc) => generateGetLogs(WebSocket, Promise, rpc);
