'use strict';

const path = require('path');

module.exports = {
  mode: 'production',
  entry: path.join(__dirname, 'browser-commonjs.js'),
  output: {
    path: __dirname,
    filename: 'browser.js',
    library: 'freelog',
    libraryTarget: 'umd',
    umdNamedDefine: true,
    globalObject: 'this'
  },
  module: {
    rules: [{
      test: /\.js$/,
      loader: 'babel-loader'
    }]
  },
  devtool: 'source-map'
};
