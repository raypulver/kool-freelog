# kool-freelog

This is a module intended to get a set of EVM logs from the blockchain over a WebSocket provider, designed to work with a stubborn RPC such as Infura.

In addition to the standard log query, you can add a `chunkSize` field to determine how many blocks you want to fetch at a time. Smaller chunk sizes are slower but more reliable to get your logs fo' free. You can also add a `concurrency` field to indicate how many concurrent WebSockets you want to spawn to do the work. This can provide quite a speedup when using a distributed cluster such as Infura.

## NodeJS Usage

```js

const freelog = require('kool-freelog'); // nodejs
// const freelog = require('kool-freelog/browser') // browser
const {
  streamLogs,
  getLogs
} = freelog('mainnet'); // freelog('wss://mainnet.infura.io/ws') // can also use freelog('ws://localhost:8545') or anything
 
// promise usage

const logs = await getLogs({
  fromBlock: 1e6,
  toBlock: 1e6 + 1000,
  topics: [ soliditySha3('Transfer(address,address,uint256)') ],
  chunkSize: 100,
  concurrency: 5
}); // returns array of raw log objects


// observable usage

const observable = streamLogs({
  fromBlock: 1e6, // can also add toBlock if we want the observable to complete, without toBlock it will continue to poll forever for new blocks
  topics: [ soliditySha3('Transfer(address,address,uint256)') ],
  chunkSize: 100,
  concurrency: 5
});

observable.subscribe({
  next({
    fromBlock,
    toBlock,
    logs
  }) {
    // do something with logs chunk, if concurrency is specified these may not be in order!
    // after all logs are fetched up to the current block, logs will be fetched 1 block at a time
  }
});

```

## TODO
- Provide ability to cancel observable
