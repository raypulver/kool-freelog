'use strict';

const addHexPrefix = (s) => s.substr(0, 2).toLowerCase() === '0x' ? s : '0x' + s;
const url = require('url');
const PromiseQueue = require('promiseq');
const numberToHex = (n) => addHexPrefix(Number(n).toString(16));
const { Observable } = require('rxjs');
const generateGetLogs = (WebSocket, Promise, network = 'mainnet') => {
  const promiseMap = {};
  let id = 0;
  const makeWsRequest = (ws, method, params = []) => {
    const current = id;
    ++id;
    return {
      promise: new Promise((resolve, reject) => {
        promiseMap[current] = {
          resolve,
          reject
        };
        ws.send(JSON.stringify({
          jsonrpc: '2.0',
          method,
          params,
          id: current
        }));
      }),
      id: current
    };
  };
	const makeSingleWsRequest = (rpc, method, params = []) => new Promise(async (resolve, reject) => {
    const parsed = url.parse(network);
    const ws = new WebSocket(parsed.protocol ? network : 'wss://' + network + '.infura.io/ws');
		const ids = [];
    const bail = async () => {
      ws.onclose = () => {};
      ids.forEach((id) => {
        delete promiseMap[id];
      });
      resolve(await makeSingleWsRequest(rpc, method, params));
    };
    const queueBail = () => setTimeout(bail, 4000);
    ws.onerror = () => bail();
    ws.onclose = () => bail();
    ws.onmessage = async (msg) => {
      try {
        const response = JSON.parse(msg.data);
        if (promiseMap[response.id]) {
          promiseMap[response.id].resolve(response.result);
          delete promiseMap[response.id];
        }
      } catch (e) {
        if (promiseMap[response.id]) {
          promiseMap[response.id].reject(e);
          delete promiseMap[response.id];
        }
      }
    };
    await new Promise((resolve) => {
      ws.onopen = () => resolve();
    });
    let timer = queueBail();
    let wsResult = makeWsRequest(ws, method, params);
    ids.push(wsResult.id);
    const result = await wsResult.promise;
    clearTimeout(timer);
    ws.onclose = () => {
      resolve(result);
    };
    try {
      ws.close();
    } catch (e) {
      resolve(logs);
    }
	});
  const getLogChunk = (o) => new Promise(async (resolve, reject) => {
    const ids = [];
    const bail = async () => {
      ws.onclose = () => {};
      ids.forEach((id) => {
        delete promiseMap[id];
      });
      resolve(await getLogChunk(o));
    };
    const queueBail = () => setTimeout(bail, 4000);
    const parsed = url.parse(network);
    const ws = new WebSocket(parsed.protocol ? network : 'wss://' + network + '.infura.io/ws');
    ws.onerror = () => bail();
    ws.onclose = () => bail();
    ws.onmessage = async (msg) => {
      try {
        const response = JSON.parse(msg.data);
        if (promiseMap[response.id]) {
          promiseMap[response.id].resolve(response.result);
          delete promiseMap[response.id];
        }
      } catch (e) {
        if (promiseMap[response.id]) {
          promiseMap[response.id].reject(e);
          delete promiseMap[response.id];
        }
      }
    };
    await new Promise((resolve) => {
      ws.onopen = () => resolve();
    });
    let timer = queueBail();
    let wsResult = makeWsRequest(ws, 'eth_newFilter', [ {
      fromBlock: numberToHex(o.fromBlock),
      toBlock: numberToHex(o.toBlock),
      address: o.address,
      topics: o.topics
    } ]);
    ids.push(wsResult.id);
    const filterId = await wsResult.promise;
    clearTimeout(timer);
    timer = queueBail();
    wsResult = makeWsRequest(ws, 'eth_getFilterLogs', [
      filterId
    ]);
    ids.push(wsResult.id);
    const logs = await wsResult.promise;
    clearTimeout(timer);
    timer = queueBail();
    wsResult = makeWsRequest(ws, 'eth_uninstallFilter', [
      filterId
    ]);
    ids.push(wsResult.id);
    await wsResult.promise;
    clearTimeout(timer);
    ws.onclose = () => {
      resolve(logs);
    };
    try {
      ws.close();
    } catch (e) {
      resolve(logs);
    }
  });
  const getLogs = async ({
    fromBlock,
    toBlock,
    address,
    topics,
    chunkSize,
    concurrency = 1
  }) => {
    const pq = new PromiseQueue(Math.ceil(Math.max(1, concurrency)));
    const logs = await Promise.all(Array.apply(null, { length: Math.ceil((Number(toBlock) - Number(fromBlock)) / chunkSize) }).map((_, i) => pq.push(() => Promise.resolve().then(() => getLogChunk({
      fromBlock: Number(fromBlock) + chunkSize*i,
      toBlock: Math.min(Number(fromBlock) + chunkSize*(i + 1), Number(toBlock)),
      address,
      topics
    })))));
    const retval = [];
    logs.forEach((v) => v.forEach((log) => retval.push(log)));
    return retval;
  };
  const streamLogs = ({
    fromBlock,
		toBlock,
    address,
    topics,
    chunkSize,
    concurrency = 1
  }) => Observable.create(async (observable) => {
		const shouldComplete = Boolean(toBlock);
		toBlock = toBlock || await makeSingleWsRequest(network, 'eth_blockNumber', []);
    const pq = new PromiseQueue(Math.ceil(Math.max(1, concurrency)));
    await Promise.all(Array.apply(null, { length: Math.ceil((Number(toBlock) - Number(fromBlock)) / chunkSize) }).map((_, i) => pq.push(() => Promise.resolve().then(() => getLogChunk({
      fromBlock: Number(fromBlock) + chunkSize*i,
      toBlock: Math.min(Number(fromBlock) + chunkSize*(i + 1), Number(toBlock)),
      address,
      topics
    }).then((logs) => observable.next({
  	  fromBlock: Number(fromBlock) + chunkSize*i,
		  toBlock: Math.min(Number(fromBlock) + chunkSize*(i + 1), Number(toBlock)),
			logs
		}))))));
		if (shouldComplete) return observable.complete();
		fromBlock = Number(toBlock);
    while (true) {
			const currentBlock = await makeSingleWsRequest(network, 'eth_blockNumber', []);
			if (currentBlock > fromBlock) {
				fromBlock++;
				observable.next({
					fromBlock,
					toBlock: fromBlock,
					logs: await getLogChunk({
						fromBlock,
						toBlock: fromBlock,
						address,
						topics
					})
				});
			}
			await new Promise((resolve) => setTimeout(resolve, 1000));
		}
  });
  return {
		getLogs,
		streamLogs
	};
};

module.exports = generateGetLogs;
